import os

from flask import Flask, render_template, request, make_response, jsonify
from flask_sqlalchemy import SQLAlchemy

from omdb import OMDBClient

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////Users/tahmidchoyon/workspace/python/newscred-omdb/database.db"
db = SQLAlchemy(app)


class Favorite(db.Model):
    __tablename__ = "favorite"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    imdbID = db.Column(db.String(15))


omdb_client = OMDBClient(apikey=os.getenv("API_KEY", "test"))


@app.route("/")
@app.route('/movies')
def home():
    args = request.args.get("search", default="godfather", type=str)

    movies = omdb_client.search_movies(args)
    return render_template("movies.html", movies=movies)


@app.route("/search")
def search():
    query = request.args.get("query", default="godfather", type=str)
    movies = omdb_client.search_movies(query)

    favs = Favorite.query.all()
    favs = [fav.imdbID for fav in favs]

    movies_list = []
    for movie in movies:
        temp = movie.to_dict()
        if movie.imdb_id in favs:
            temp["favorite"] = True
        movies_list.append(temp)

    return make_response(jsonify({
        "result": movies_list
    }), 200)


@app.route("/movies/favorite", methods=["POST"])
def favorite_movies():
    data = request.get_json()
    if data.get("imdbID"):
        fav = Favorite(imdbID=data.get("imdbID"))
        db.session.add(fav)
        db.session.commit()
    return make_response(jsonify({
        "msg": "success"
    }), 201)


if __name__ == '__main__':
    app.run()
