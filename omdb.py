from typing import Dict, List

import requests
from requests.models import Response

from models import MovieFactory, Movie


class OMDBClient(object):
    _client = None

    def __init__(self, apikey: str):
        if not apikey:
            raise Exception("OMDB API KEY is not present")
        self._apikey = apikey
        self._base_url: str = f"http://www.omdbapi.com/?apikey={self._apikey}"
        self._page: int = 1

    def _prepare_params(self, s: str) -> str:
        return "s=" + s.replace(" ", "+").lower() + "&" + f"page={self._page}"

    def prepare_url(self, s: str) -> str:
        return f"{self._base_url}&{self._prepare_params(s)}"

    def _make_request(self, url: str) -> Response:
        print(url)
        response = requests.get(url)
        response.raise_for_status()
        return response

    def search_string(self, s: str) -> Dict:
        url = self.prepare_url(s)
        response = self._make_request(url)
        json_res = response.json()
        assert json_res["Response"] == "True", f"invalid response from omdb api, error: {json_res['Error']}"
        return json_res

    def search_movies(self, s: str) -> List[Movie]:
        data = self.search_string(s)
        if "Search" not in data:
            return []

        movies = MovieFactory.get_movies(data["Search"])
        return movies

    def next_page(self, s: str) -> List[Movie]:
        self._page += 1
        url = self.prepare_url(s)
        return self.search_movies(url)
